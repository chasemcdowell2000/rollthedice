let wrapper = document.createElement("div");
wrapper.className = "wrapper"
let count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,];
function dice() {
    for (let counter = 0; counter < 1000; counter++) {
        let x = Math.random();
        let y = x * 6;
        let z = Math.ceil(y);
        let a = Math.random();
        let b = a * 6;
        let c = Math.ceil(b);
        i = z + c;
        count[i] = count[i] + 1;
    }
    for (let i = 2; i <= 12; i++) {
        writeToDOM(i + "'s rolled:", count[i])
    }
}

function writeToDOM(title, numbers) {
    let div = document.createElement("div");
    div.className = "answer";
    div.style.width = numbers + "0px";
    let heading = document.createElement("h4");
    let titleText = document.createTextNode(title);
    let answerText = document.createTextNode(numbers);
    heading.appendChild(titleText);
    div.appendChild(heading);
    div.appendChild(answerText);
    wrapper.appendChild(div);
}
var destination = document.getElementById("content");
destination.appendChild(wrapper);

dice()
